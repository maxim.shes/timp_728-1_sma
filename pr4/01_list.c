#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct node {
    int value;          // значение, которое хранит узел
    struct node *next;  // ссылка на следующий элемент списка
    struct node *prev;  // ссылка на предыдущий элемент списка
} node;

typedef struct list {
    struct node *head;  // начало списка
    struct node *tail;  // конец спискаnext
} list;


// инициализация пустого списка
void init(list *l) {
    l->head = NULL;
    l->tail = NULL;
}

// удалить все элементы из списка
void clean(list *l) {
    l->head = NULL;
    l->tail = NULL;
}

// проверка на пустоту списка
bool is_empty(list *l) {
    if (l->head == NULL) {
        return true;
    } else return false;
}

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value) {
    node *position = l->head;
    while (position != NULL) {
        if (value == position->value) {
            return position;
        }
        position = position->next;
    }
    return NULL;
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value) {
    if (l->head == NULL) {
        l->head = malloc(sizeof(node));
        l->head->next = NULL;
        l->head->prev = NULL;
        l->tail = l->head;
        l->head->value = value;
        return 0;
    } else {
        node *position = l->tail;
        position->next = malloc(sizeof(node));
        l->tail = position->next;
        position->next->prev = position;
        position->next->value = value;
        position->next->next = NULL;
        return 0;
    }
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value) {
    if (l->head == NULL) {
        l->head = malloc(sizeof(node));
        l->head->next = NULL;
        l->head->prev = NULL;
        l->tail = l->head;
        l->head->value = value;
        return 0;
    } else {
        node *new_node;
        new_node = malloc(sizeof(node));
        new_node->value = value;
        new_node->next = l->head;
        l->head->prev = new_node;
        l->head = new_node;
        return 0;
    }
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, node *n, int after) {
    if (after == 0) {
        n->next = l->head;
        n->prev = NULL;
        l->head->prev = n;
        l->head = n;
        return 0;
    } else {
        node *position = l->head;
        int i = 1;
        while (position != NULL && i < after) {
            position = position->next;
            i++;
        }
        if (i == after) {
            if (position->next == NULL) {
                position->next = n;
                n->prev = position;
                n->next = NULL;
                l->tail = n;
            } else {
                n->prev = position;
                n->next = position->next;
                position->next = n;
                n->next->prev = n;
                return 0;
            }
        }
        return 1;
    }
}

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list *l, node *n, int before) {
    if (before == 1) {
        n->next = l->head;
        n->prev = NULL;
        l->head->prev = n;
        l->head = n;
    } else {
        node *position = l->head;
        int i = 2;
        while (position != NULL && i < before) {
            position = position->next;
            i++;
        }
        if (i == before && position != NULL) {
            if (position->next == NULL) {
                position->next = n;
                n->prev = position;
                n->next = NULL;
                l->tail = n;
            } else {
                n->prev = position;
                n->next = position->next;
                position->next = n;
                n->next->prev = n;
                return 0;
            }
        }
        return 1;
    }
}

// удалить первый элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_first(list *l, int value) {
    if (!is_empty(l)) {
        if (l->head->value == value) {
            l->head->next->prev=NULL;
            l->head = l->head->next;
            return 0;
        } else {
            node *position = l->head;
            while (position != NULL && value != position->value) {
                position = position->next;
            }
            if (position != NULL && value == position->value) {
                if (position->next == NULL) {
                    l->tail = position->prev;
                    position->prev->next = NULL;
                    return 0;
                } else {
                    position->prev->next = position->next;
                    position->next->prev = position->prev;
                    return 0;
                }
            }
        }
    }
    return 1;
}

// удалить последний элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_last(list *l, int value) {
    if (!is_empty(l)) {
        if (l->tail->value == value) {
            l->tail->prev->next=NULL;
            l->tail = l->tail->prev;
            return 0;
        } else {
            node *position = l->tail;
            while (position != NULL && value != position->value) {
                position = position->prev;
            }
            if (position != NULL && value == position->value) {
                if (position->prev == NULL) {
                    l->head = position->next;
                    position->next->prev = NULL;
                    return 0;
                } else {
                    position->prev->next = position->next;
                    position->next->prev = position->prev;
                    return 0;
                }
            }
        }
    }
    return 1;
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l) {
    node *position = l->head;
    while (position != NULL) {
        printf("%d ", position->value);
        position = position->next;
    }
    printf("\n");
}

// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list *l) {
    node *position = l->tail;
    while (position != NULL) {
        printf("%d ", position->value);
        position = position->prev;
    }
    printf("\n");
}



int main() {
    list list;
    node node1, *nodep,node2;
    int n, i, temp, a, b, c;
    init(&list);

    (void) scanf("%d", &n);
    for (i = 0; i < n; i++) {
        (void) scanf("%d", &temp);
        push_back(&list, temp);
    }
    print(&list);

    (void) scanf("%d%d%d",&a,&b,&c);
    printf("%d %d %d\n",find(&list,a)!=NULL,find(&list,b)!=NULL,find(&list,c)!=NULL);

    (void) scanf("%d", &n);
    push_back(&list,n);
    print_invers(&list);

    (void) scanf("%d", &n);
    push_front(&list,n);
    print(&list);

    (void) scanf("%d%d", &a,&b);
    node1.value=b;
    insert_after(&list,&node1,a);
    print_invers(&list);

    (void) scanf("%d%d", &a,&b);
    node2.value=b;
    insert_before(&list,&node2,a);
    print(&list);

    (void) scanf("%d", &n);
    remove_first(&list,n);
    print_invers(&list);

    (void) scanf("%d", &n);
    remove_last(&list,n);
    print(&list);

    clean(&list);
    
    return 0;
}
