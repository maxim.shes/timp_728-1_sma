#include <stdio.h>
#include <math.h>

int main()
{
    int n,a;
    double b,c=0;
    (void) scanf("%d", &n);
    for(a=1;a<=n;a++)
    {
        (void) scanf("%lf", &b);
        c=c + (((a + 1) % 2) * pow (b,3));
    }
    printf ("%lf",c);
    return 0;
}