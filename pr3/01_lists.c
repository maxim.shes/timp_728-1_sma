#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct node {
    int value;          // значение, которое хранит узел
    struct node *next;  // ссылка на следующий элемент списка
} node;

typedef struct list {
    struct node *head;  // начало списка
} list;


// инициализация пустого списка
void init(list *l) {
    l->head = NULL;
}

// проверка на пустоту списка
bool is_empty(list *l) {
    if (l->head == NULL) {
        return true;
    } else return false;
}

// удалить все элементы из списка
void clean(list *l) {
    l->head = NULL;
}

// поиск элемента по значению. вернуть 0 если эжемент не найден
int find(list *l, int value) {
    node *position = l->head;
    while (position != NULL) {
        if (value == position->value) {
            return 1;
        }
        position = position->next;
    }
    return 0;
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l,int after, int value) {
    if (after == 0) {
        node *new_node;
        new_node = malloc(sizeof(node));
        new_node->value = value;
        new_node->next = l->head;
        l->head = new_node;
        return 0;
    } else {
        node *position = l->head;
        int i = 1;
        while (position != NULL && i < after) {
            position=position->next;
            i++;
        }
        if (i == after) {
            node *new_node;
            new_node = malloc(sizeof(node));
            new_node->value = value;
            new_node->next = position->next;
            position->next = new_node;
            return 0;
        }
        return 1;
    }
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value) {
    if (l->head == NULL) {
        l->head = malloc(sizeof(node));
        l->head->next = NULL;
        l->head->value = value;
        return 0;
    } else {
        node *position = l->head;
        while (position->next != NULL) {
            position = position->next;
        }
        position->next = malloc(sizeof(node));
        position->next->value = value;
        position->next->next = NULL;
        return 0;
    }
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value) {
    if (l->head == NULL) {
        l->head = malloc(sizeof(node));
        l->head->next = NULL;
        l->head->value = value;
        return 0;
    } else {
        node *new_node;
        new_node = malloc(sizeof(node));
        new_node->value = value;
        new_node->next = l->head;
        l->head = new_node;
        return 0;
    }
}

// удалить первый элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_node(list *l, int value) {
    if (l->head->value == value) {
        l->head = l->head->next;
        return 0;
    } else {
        node *position = l->head, *temp = position;
        while (position != NULL && value != position->value) {
            position = position->next;
        }

        if (position!=NULL&&value == position->value) {
            while (temp->next != position) {
                temp = temp->next;
            }
            temp->next = position->next;
            free(position);
            return 0;
        }
    }
    return 1;
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l) {
    node *position = l->head;
    while (position != NULL) {
        printf("%d ", position->value);
        position = position->next;
    }
    printf("\n");
}


int main() {
    list list;
    int n, i, temp,a,b,c;
    init(&list);

    (void) scanf("%d", &n);
    for (i = 0; i < n; i++) {
        (void) scanf("%d", &temp);
        push_back(&list, temp);
    }
    print(&list);

    (void) scanf("%d%d%d",&a,&b,&c);
    printf("%d %d %d\n",find(&list,a),find(&list,b),find(&list,c));

    (void) scanf("%d",&a);
    push_back(&list,a);
    print(&list);

    (void) scanf("%d",&a);
    push_front(&list,a);
    print(&list);

    (void) scanf("%d%d",&a,&b);
    insert_after(&list,a,b);
    print(&list);

    (void) scanf("%d",&a);
    remove_node(&list,a);
    print(&list);

    return 0;
}


