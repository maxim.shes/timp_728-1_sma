#include <stdio.h>
#include <stdlib.h>

// Структура для хранения узла дерева.
// Необходимо хранить ссылки на потомков, предка и некоторое значение
typedef struct node {
	int value;
	struct node* left;
	struct node* righ;
} node;


// Структура для хранения дерева.
// Хранит ссылку на корень дерева и количество элементов в дереве
typedef struct tree {
	struct node* root;
} tree;


// Инициализация дерева
void init(tree* t) {
	t->root = NULL;
};


int insertnode(node* n, int value) {
	if (n != NULL) {
		if (value == n->value) {
			return 1;
		}
		if (n->value < value) {
			if (n->righ == NULL) {
				n->righ = malloc(sizeof(node));
				n->righ->value = value;
				n->righ->righ = NULL;
				n->righ->left = NULL;
				return 0;
			}
			return insertnode(n->righ, value);
		}
		else {
			if (n->left == NULL) {
				n->left = malloc(sizeof(node));
				n->left->value = value;
				n->left->left = NULL;
				n->left->righ = NULL;
				return 0;
			}
			return insertnode(n->left, value);
		}
	}
	else {
		n = malloc(sizeof(node));
		n->value = value;
		n->left = NULL;
		n->righ = NULL;
		return 0;
	}
};

// Вставка значения в дерево:
// 0 - вставка выполнена успешно
// 1 - элемент существует
// 2 - не удалось выделить память для нового элемента
int insert(tree* t, int value) {
	if (t->root == NULL) {
		t->root = malloc(sizeof(node));
		t->root->value = value;
		t->root->left = NULL;
		t->root->righ = NULL;
		return 0;
	}
	else {
		return insertnode(t->root, value);
	}

};

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct qnode {
    struct node* value;          // значение, которое хранит узел
    struct qnode* next;  // ссылка на следующий элемент списка
    struct qnode* prev;  // ссылка на предыдущий элемент списка
} qnode;

typedef struct queue {
    struct qnode* head;  // начало списка
    struct qnode* tail;  // конец спискаnext
} queue;


// инициализация пустого списка
void initq(queue* l) {
    l->head = NULL;
    l->tail = NULL;
}


// проверка на пустоту списка
bool is_empty(queue* l) {
    if (l->head == NULL) {
        return true;
    }
    else return false;
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(queue* l, node* value) {
    if (l->head == NULL) {
        l->head = malloc(sizeof(qnode));
        l->head->next = NULL;
        l->head->prev = NULL;
        l->tail = l->head;
        l->head->value = value;
        return 0;
    }
    else {
        qnode* position = l->tail;
        position->next = malloc(sizeof(qnode));
        l->tail = position->next;
        position->next->prev = position;
        position->next->value = value;
        position->next->next = NULL;
        return 0;
    }
}




// удалить последний элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_first(queue* l, int value) {
    if (!is_empty(l)) {
        if (l->head->value->value == value) {
            if (l ->head->next == NULL) {
                l->head = NULL;
                l->tail = NULL;
            }
            else {
                l->head->next->prev = NULL;
                l->head = l->head->next;
                return 0;
            }
        }
        else {
            qnode* position = l->head;
            while (position != NULL && value != position->value->value) {
                position = position->next;
            }
            if (position != NULL && value == position->value->value) {
                if (position->next == NULL) {
                    l->tail = position->prev;
                    position->prev->next = NULL;
                    //free(position);
                    return 0;
                }
                else {
                    position->prev->next = position->next;
                    position->next->prev = position->prev;
                    //free(position);
                    return 0;
                }
            }
        }
    }
    return 1;
}




int main() {
	tree tree;
    queue q;
	node* node1, * node2;
	init(&tree);
    initq(&q);
	int a;

	for (int i = 0; i < 7; i++) {
		(void)scanf("%d", &a);
		insert(&tree, a);
	}

    push_back(&q, tree.root);
    while (!is_empty(&q)) {
        node1 = q.tail->value;
        remove_first(&q, q.tail->value->value);
        printf("%d ", node1->value);
        if (node1->righ != NULL) {
            push_back (&q, node1->righ);
        }
        if (node1->left != NULL) {
            push_back(&q, node1->left);
        }
    }
};
